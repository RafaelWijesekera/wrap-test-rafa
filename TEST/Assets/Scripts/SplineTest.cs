﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;
using UnityEngine.UI;


public class SplineTest : MonoBehaviour
{
    // Start is called before the first frame update

    public SplineComputer spline;
    public PathGenerator renderer;

    private int count = 5;
    

    private List<SplinePoint> points;
    void Start()
    {

        points = new List<SplinePoint>();

        for (int i = 0; i < 5; i++)
        {
            var point = new SplinePoint();
            point.position = Vector3.forward * i;
            point.normal = Vector3.up;
            point.size = 1f;
            point.color = Color.white;
            
            points.Add(point);
        }

        spline.SetPoints(points.ToArray());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            count++;

            var point = new SplinePoint();
            point.position = Vector3.forward * count ;
            point.normal = Vector3.up;
            point.size = 1f;
            point.color = Color.white;
            points.Add(point);

            spline.SetPoint(count, point);
        }
        
        
    }
}
