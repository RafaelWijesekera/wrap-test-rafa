﻿using System;
using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapTheRope1 : MonoBehaviour
{
    public SplineComputer spline;
    public Transform rollAttachPoint;
    public Transform lastObjectAttachPoint;

    public LayerMask layerMask;


    public SplinePoint[] points = new SplinePoint[2];
    
    public List<SplinePoint> pointsList = new List<SplinePoint>();
    
    public List<Transform> pointTransforms = new List<Transform>();

    [Range(0, 4)]
    public float ropeThiccccness = 1;
    private void Awake()
    {
        //StartCoroutine(WrapTheRope());
    }

    private void Start()
    {
        pointsList = new List<SplinePoint>();
        pointTransforms = new List<Transform>();
        
        
        var point = new SplinePoint();
        point.position = lastObjectAttachPoint.transform.position;
        point.normal = Vector3.up;
        point.size = 1f;
        point.color = Color.white;
            
        pointsList.Add(point);
        
        
        point = new SplinePoint();
        point.position = rollAttachPoint.transform.position;
        point.normal = Vector3.up;
        point.size = 1f;
        point.color = Color.white;
            
        pointsList.Add(point);
        
        
        pointTransforms.Add(lastObjectAttachPoint.transform);
        

        
        
        
        spline.SetPoints(pointsList.ToArray());
    }

    private void Update()
    {
        UpdatePoints();
    }

    private void UpdatePoints()
    {

        var count = 0;
        foreach (var point in pointTransforms)
        {
            pointsList[count] = SetPoint(point.position);
            
            
            spline.SetPoint(count, pointsList[count]);
            
            count++;
        }

        
    }

    private IEnumerator WrapTheRope()
    {
        points = new SplinePoint[2];

        //intializing the first two points;
        points[0] = SetPoint(lastObjectAttachPoint.position, lastObjectAttachPoint.up);
        points[1] = SetPoint(rollAttachPoint.position, rollAttachPoint.up);

        while (true) {
            if (Input.GetMouseButton(0) || true)
            {

                RaycastHit hit;

                Debug.DrawLine(rollAttachPoint.position, lastObjectAttachPoint.position, Color.blue);

                Debug.DrawLine(lastObjectAttachPoint.position, rollAttachPoint.position, Color.red);
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(lastObjectAttachPoint.position, rollAttachPoint.position, out hit, Mathf.Infinity, layerMask))
                {
                    //Set the attach new attach point

                    //retrieve the previous points
                    SplinePoint[] newPoints = new SplinePoint[points.Length + 1];
                    System.Array.Copy(points, newPoints, points.Length - 1);

                    //new point one before last
                    newPoints[points.Length - 2] = SetPoint(hit.point, hit.normal);
                    //lastPoint
                    newPoints[points.Length - 1] = SetPoint(rollAttachPoint.position, rollAttachPoint.up);

                    points = newPoints;
                    spline.SetPoints(points, SplineComputer.Space.World);
                    Debug.Log("Point");

                    //Transform newPoint = Instantiate(lastObjectAttachPoint);
                    //newPoint.transform.parent = spline.transform;
                    //newPoint.transform.position = hit.point;
                    lastObjectAttachPoint.position = hit.point;
                }
            }
            yield return null;
        }
    }

    public Transform sphere;

    SplinePoint SetPoint(Vector3 pos)
    {
        var point = new SplinePoint();
        point.position = pos;
        point.normal = Vector3.up;
        point.size = 1f;
        point.color = Color.white;

        return point;
    }

    SplinePoint SetPoint(Vector3 pos, Vector3 normal)
    {
        SplinePoint point = new SplinePoint();

        point.position = rollAttachPoint.position;
        point.normal = rollAttachPoint.up;

        point.position = spline.transform.InverseTransformPoint(rollAttachPoint.position);
        point.normal = spline.transform.InverseTransformDirection(rollAttachPoint.up);
        point.size = ropeThiccccness;
        point.color = Color.white;
        return point;
    }

    private void RefreshSplineComputer()
    {
        spline.SetPoints(points);
    }

    

    ////TODO:
    ///Set up the first two points for the rope
    ///Check if this works with a splinecomputer
    ///
}
